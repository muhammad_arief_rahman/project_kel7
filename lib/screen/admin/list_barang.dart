import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/dialog/dialog_route.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/barang.dart';
import 'package:project_kel_7/screen/admin/add_update_barang.dart';

class ListBarang extends StatefulWidget {
  @override
  State<ListBarang> createState() => _ListBarangState();
}

class _ListBarangState extends State<ListBarang> {
  List<Barang> _ListBarang = [];

  void getBarang() async {
    _ListBarang = await Eventdb.getBarang();

    setState(() {});
  }

  @override
  void initState() {
    getBarang();
    super.initState();
  }

  void showOption(Barang? barang) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdateBarang(barang: barang))?.then((value) => getBarang());
        break;
      case 'delete':
        Eventdb.deleteBarang(barang!.kodeBarang!).then((value) => getBarang());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //titleSpacing: 0,
        title: Text('List Barang'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _ListBarang.length > 0
              ? ListView.builder(
                  itemCount: _ListBarang.length,
                  itemBuilder: (context, index) {
                    Barang barang = _ListBarang[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(barang.namaBarang ?? ''),
                      subtitle: Text(barang.kodeBarang ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(barang),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdateBarang())?.then((value) => getBarang()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
