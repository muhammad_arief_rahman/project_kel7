import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/pengajuan.dart';
import 'package:project_kel_7/screen/admin/add_update_pengajuan.dart';

class ListPengajuan extends StatefulWidget {
  @override
  State<ListPengajuan> createState() => _ListPengajuanState();
}

class _ListPengajuanState extends State<ListPengajuan> {
  List<Pengajuan> _ListPengajuan = [];

  void getPengajuan() async {
    _ListPengajuan = await Eventdb.getPengajuan();

    setState(() {});
  }

  @override
  void initState() {
    getPengajuan();
    super.initState();
  }

  void showOption(Pengajuan? pengajuan) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengajuan(pengajuan: pengajuan))
            ?.then((value) => getPengajuan());
        break;
      case 'delete':
        Eventdb.deletePengajuan(pengajuan!.kodePengajuan!)
            .then((value) => getPengajuan());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        title: Text('List Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _ListPengajuan.length > 0
              ? ListView.builder(
                  itemCount: _ListPengajuan.length,
                  itemBuilder: (context, index) {
                    Pengajuan pengajuan = _ListPengajuan[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengajuan.namaPeminjam ?? ''),
                      subtitle: Text(pengajuan.npmPeminjam ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengajuan),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () =>
                  Get.to(AddUpdatePengajuan())?.then((value) => getPengajuan()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
