import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/pengajuan.dart';
import 'package:project_kel_7/screen/admin/list_pengajuan.dart';
import 'package:project_kel_7/widget/info.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final Pengajuan? pengajuan;
  AddUpdatePengajuan({this.pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengajuan = TextEditingController();
  var _controllertanggal = TextEditingController();
  var _controllernpmPeminjam = TextEditingController();
  var _controllernamaPeminjam = TextEditingController();
  var _controllerprodi = TextEditingController();
  var _controllernoHandphone = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengajuan != null) {
      _controllerkodePengajuan.text = widget.pengajuan!.kodePengajuan!;
      _controllertanggal.text = widget.pengajuan!.tanggal!;
      _controllernpmPeminjam.text = widget.pengajuan!.npmPeminjam!;
      _controllernamaPeminjam.text = widget.pengajuan!.namaPeminjam!;
      _controllerprodi.text = widget.pengajuan!.prodi!;
      _controllernoHandphone.text = widget.pengajuan!.noHandphone!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengajuan != null
            ? Text('Update pengajuan')
            : Text('Tambah pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggal,
                  decoration: InputDecoration(
                      labelText: "tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernpmPeminjam,
                  decoration: InputDecoration(
                      labelText: "NPM",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernamaPeminjam,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerprodi,
                  decoration: InputDecoration(
                      labelText: "Program Studi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernoHandphone,
                  decoration: InputDecoration(
                      labelText: "No Handphone",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengajuan == null) {
                        String message = await Eventdb.addPengajuan(
                          _controllerkodePengajuan.text,
                          _controllertanggal.text,
                          _controllernpmPeminjam.text,
                          _controllernamaPeminjam.text,
                          _controllerprodi.text,
                          _controllernoHandphone.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengajuan.clear();
                          _controllertanggal.clear();
                          _controllernpmPeminjam.clear();
                          _controllernamaPeminjam.clear();
                          _controllerprodi.clear();
                          _controllernoHandphone.clear();
                          Get.off(
                            ListPengajuan(),
                          );
                        }
                      } else {
                        Eventdb.updatePengajuan(
                          _controllerkodePengajuan.text,
                          _controllertanggal.text,
                          _controllernpmPeminjam.text,
                          _controllernamaPeminjam.text,
                          _controllerprodi.text,
                          _controllernoHandphone.text as int,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
