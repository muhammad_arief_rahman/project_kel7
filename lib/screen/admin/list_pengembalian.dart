import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/pengembalian.dart';
import 'package:project_kel_7/screen/admin/add_update_pengembalian.dart';

class ListPengembalian extends StatefulWidget {
  @override
  State<ListPengembalian> createState() => _ListPengembalianState();
}

class _ListPengembalianState extends State<ListPengembalian> {
  List<Pengembalian> _ListPengembalian = [];

  void getPengembalian() async {
    _ListPengembalian = await Eventdb.getPengembalian();

    setState(() {});
  }

  @override
  void initState() {
    getPengembalian();
    super.initState();
  }

  void showOption(Pengembalian? Pengembalian) async {
    var result = await Get.dialog(
        SimpleDialog(
          children: [
            ListTile(
              onTap: () => Get.back(result: 'update'),
              title: Text('Update'),
            ),
            ListTile(
              onTap: () => Get.back(result: 'delete'),
              title: Text('Delete'),
            ),
            ListTile(
              onTap: () => Get.back(),
              title: Text('Close'),
            )
          ],
        ),
        barrierDismissible: false);
    switch (result) {
      case 'update':
        Get.to(AddUpdatePengembalian(pengembalian: Pengembalian))
            ?.then((value) => getPengembalian());
        break;
      case 'delete':
        Eventdb.deletePengembalian(Pengembalian!.kodePengembalian!)
            .then((value) => getPengembalian());
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        title: Text('List Pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          _ListPengembalian.length > 0
              ? ListView.builder(
                  itemCount: _ListPengembalian.length,
                  itemBuilder: (context, index) {
                    Pengembalian pengembalian = _ListPengembalian[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Text('${index + 1}'),
                        backgroundColor: Colors.white,
                      ),
                      title: Text(pengembalian.tanggalKembali ?? ''),
                      subtitle: Text(pengembalian.kodePengembalian ?? ''),
                      trailing: IconButton(
                          onPressed: () => showOption(pengembalian),
                          icon: Icon(Icons.more_vert)),
                    );
                  },
                )
              : Center(
                  child: Text("Data Kosong"),
                ),
          Positioned(
            bottom: 16,
            right: 16,
            child: FloatingActionButton(
              onPressed: () => Get.to(AddUpdatePengembalian())
                  ?.then((value) => getPengembalian()),
              child: Icon(Icons.add),
              backgroundColor: Asset.colorAccent,
            ),
          )
        ],
      ),
    );
  }
}
