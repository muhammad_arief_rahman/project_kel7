import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/barang.dart';
import 'package:project_kel_7/screen/admin/list_barang.dart';
import 'package:project_kel_7/widget/info.dart';

class AddUpdateBarang extends StatefulWidget {
  final Barang? barang;
  AddUpdateBarang({this.barang});

  @override
  State<AddUpdateBarang> createState() => _AddUpdateBarangState();
}

class _AddUpdateBarangState extends State<AddUpdateBarang> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodeBarang = TextEditingController();
  var _controllernamaBarang = TextEditingController();
  var _controllerjumlah = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.barang != null) {
      _controllerkodeBarang.text = widget.barang!.kodeBarang!;
      _controllernamaBarang.text = widget.barang!.namaBarang!;
      _controllerjumlah.text = widget.barang!.jumlah!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        titleSpacing: 0,
        title: widget.barang != null
            ? Text('Update barang')
            : Text('Tambah barang'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.barang == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodeBarang,
                  decoration: InputDecoration(
                      labelText: "Kode Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernamaBarang,
                  decoration: InputDecoration(
                      labelText: "Nama Barang",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerjumlah,
                  decoration: InputDecoration(
                      labelText: "jumlah",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.barang == null) {
                        String message = await Eventdb.addBarang(
                            _controllerkodeBarang.text,
                            _controllernamaBarang.text,
                            _controllerjumlah.text);
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodeBarang.clear();
                          _controllernamaBarang.clear();
                          _controllerjumlah.clear();
                          Get.off(
                            ListBarang(),
                          );
                        }
                      } else {
                        Eventdb.updateBarang(
                          _controllerkodeBarang.text,
                          _controllernamaBarang.text,
                          _controllerjumlah.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.barang == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
