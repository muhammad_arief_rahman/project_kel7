import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:project_kel_7/config/asset.dart';
import 'package:project_kel_7/event/event_db.dart';
import 'package:project_kel_7/model/pengembalian.dart';
import 'package:project_kel_7/screen/admin/add_update_pengajuan.dart';
import 'package:project_kel_7/screen/admin/list_pengembalian.dart';
import 'package:project_kel_7/widget/info.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? pengembalian;
  AddUpdatePengembalian({this.pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkodePengembalian = TextEditingController();
  var _controllerkodePengajuan = TextEditingController();
  var _controllertanggalKembali = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian != null) {
      _controllerkodePengembalian.text = widget.pengembalian!.kodePengembalian!;
      _controllerkodePengajuan.text = widget.pengembalian!.kodePengajuan!;
      _controllertanggalKembali.text = widget.pengembalian!.tanggalKembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembalian != null
            ? Text('Update pengajuan')
            : Text('Tambah pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertanggalKembali,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian == null) {
                        String message = await Eventdb.addPengembalian(
                            _controllerkodePengembalian.text,
                            _controllerkodePengajuan.text,
                            _controllertanggalKembali.text);
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkodePengembalian.clear();
                          _controllertanggalKembali.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        Eventdb.updatePengembalian(
                            _controllerkodePengembalian.text,
                            _controllerkodePengajuan.text,
                            _controllertanggalKembali.text);
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
