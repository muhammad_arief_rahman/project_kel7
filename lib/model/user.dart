import 'package:project_kel_7/controller/c_user.dart';

class User {
  String? iduser;
  String? name;
  String? userName;
  String? pass;
  String? role;

  User({this.iduser, this.name, this.userName, this.pass, this.role});

  factory User.fromJson(Map<String, dynamic> json) => User(
        iduser: json['id_user'],
        name: json['name'],
        userName: json['userName'],
        pass: json['pass'],
        role: json['role'],
      );

  Map<String, dynamic> toJson() => {
        'id_user': this.iduser,
        'name': this.name,
        'userName': this.userName,
        'pass': this.pass,
        'role': this.role,
      };
}
