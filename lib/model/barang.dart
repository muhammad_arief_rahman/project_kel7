import 'package:project_kel_7/controller/c_Barang.dart';

class Barang {
  String? kodeBarang;
  String? namaBarang;
  String? jumlah;

  Barang({this.kodeBarang, this.namaBarang, this.jumlah});

  factory Barang.fromJson(Map<String, dynamic> json) => Barang(
        kodeBarang: json['kodeBarang'],
        namaBarang: json['namaBarang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kodeBarang': this.kodeBarang,
        'namaBarang': this.namaBarang,
        'jumlah': this.jumlah,
      };
}
