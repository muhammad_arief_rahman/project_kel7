import 'package:project_kel_7/controller/c_Pengajuan.dart';

class Pengajuan {
  String? kodePengajuan;
  String? tanggal;
  String? npmPeminjam;
  String? namaPeminjam;
  String? prodi;
  String? noHandphone;

  Pengajuan(
      {this.kodePengajuan,
      this.tanggal,
      this.npmPeminjam,
      this.namaPeminjam,
      this.prodi,
      this.noHandphone});

  factory Pengajuan.fromJson(Map<String, dynamic> json) => Pengajuan(
      kodePengajuan: json['kodePengajuan'],
      tanggal: json['tanggal'],
      npmPeminjam: json['npmPeminjam'],
      namaPeminjam: json['namaPeminjam'],
      prodi: json['prodi'],
      noHandphone: json['noHandphone']);

  Map<String, dynamic> toJson() => {
        'kodePengajuan': this.kodePengajuan,
        'tanggal': this.tanggal,
        'npmPeminjam': this.npmPeminjam,
        'namaPeminjam': this.namaPeminjam,
        'prodi': this.prodi,
        'noHandphone': this.noHandphone,
      };
}
