class Pengembalian {
  String? kodePengembalian;
  String? kodePengajuan;
  String? tanggalKembali;

  Pengembalian(
      {this.kodePengembalian, this.kodePengajuan, this.tanggalKembali});

  factory Pengembalian.fromJson(Map<String, dynamic> json) => Pengembalian(
      kodePengembalian: json['kodePengembalian'],
      kodePengajuan: json['kodePengajuan'],
      tanggalKembali: json['tanggalKembali']);

  Map<String, dynamic> toJson() => {
        'kodePengembalian': this.kodePengembalian,
        'kodePengajuan': this.kodePengajuan,
        'tanggalKembali': this.tanggalKembali
      };

  static void forEach(Null Function(dynamic Pengembalian) param0) {}
}
