import 'dart:ui';

class Asset {
  static Color colorPrimaryDark = Color.fromARGB(255, 112, 5, 9);
  static Color colorPrimary = Color.fromARGB(255, 90, 1, 4);
  static Color colorSecondary = Color.fromARGB(255, 155, 19, 19);
  static Color colorAccent = Color.fromARGB(255, 155, 19, 19);
}
