import 'dart:convert';

import 'package:project_kel_7/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EventPref {
  static void saveUser(User user) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString('user', jsonEncode(user));
  }

  static Future<User?> getUser() async {
    User? user;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? stringuser = pref.getString('User');

    if (stringuser != null) {
      Map<String, dynamic> mapUser = jsonDecode(stringuser);
    }

    return user;
  }

  static void clear() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }
}
