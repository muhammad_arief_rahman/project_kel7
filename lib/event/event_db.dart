import 'dart:convert';

import 'package:get/get.dart';
import 'package:project_kel_7/config/api.dart';
import 'package:project_kel_7/event/event_pref.dart';
import 'package:project_kel_7/model/barang.dart';
import 'package:http/http.dart' as http;
import 'package:project_kel_7/screen/admin/add_update_barang.dart';
import 'package:project_kel_7/model/pengajuan.dart';
import 'package:project_kel_7/model/pengembalian.dart';
import 'package:project_kel_7/model/user.dart';
import 'package:project_kel_7/screen/login.dart';
import 'package:project_kel_7/widget/info.dart';

class Eventdb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              Login(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUser));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> updateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['barang'];

          barang.forEach((barang) {
            listBarang.add(Barang.fromJson(barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }

  static Future<String> addBarang(
      String kodeBarang, String namaBarang, String jumlah) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kodeBarang': kodeBarang,
        'namaBarang': namaBarang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> updateBarang(
      String kodeBarang, String namaBarang, String jumlah) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kodeBarang': kodeBarang,
        'namaBarang': namaBarang,
        'jumlah': jumlah
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Barang');
        } else {
          Info.snackbar('Gagal Update Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteBarang(String kodeBarang) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteBarang), body: {'kodeBarang': kodeBarang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Barang');
        } else {
          Info.snackbar('Gagal Delete Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengajuan>> getPengajuan() async {
    List<Pengajuan> listPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuan = responBody['pengajuan'];

          pengajuan.forEach((pengajuan) {
            listPengajuan.add(Pengajuan.fromJson(pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuan;
  }

  static Future<String> addPengajuan(
      String kodePengajuan,
      String tanggal,
      String npmPeminjam,
      String namaPeminjam,
      String prodi,
      String noHandphone) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuan), body: {
        'kodePengajuan': kodePengajuan,
        'tanggal': tanggal,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'prodi': prodi,
        'noHandphone': noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> updatePengajuan(
      String kodePengajuan,
      String tanggal,
      String npmPeminjam,
      String namaPeminjam,
      String prodi,
      int noHandphone) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuan), body: {
        'kodePengajuan': kodePengajuan,
        'tanggal': tanggal,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'prodi': prodi,
        'noHandphone': noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuan');
        } else {
          Info.snackbar('Gagal Update Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengajuan(String kodePengajuan) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengajuan),
          body: {'kodePengajuan': kodePengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Menghapus Pengajuan');
        } else {
          Info.snackbar('Gagal Menghapus Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Pengembalian>> getPengembalian() async {
    List<Pengembalian> listPengembalian = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengembalian));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengembalian = responBody['pengembalian'];

          pengembalian.forEach((pengembalian) {
            listPengembalian.add(Pengembalian.fromJson(pengembalian));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengembalian;
  }

  static Future<String> addPengembalian(String kodePengembalian,
      String kodePengajuan, String tanggalKembali) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengembalian), body: {
        'kodePengembalian': kodePengembalian,
        'kodePengajuan': kodePengajuan,
        'tanggalKembali': tanggalKembali
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengembalian Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> updatePengembalian(String kodePengembalian,
      String kodePengajuan, String tanggalKembali) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengembalian), body: {
        'kode_pengembalian': kodePengembalian,
        'kodePengajuan': kodePengajuan,
        'tanggal_kembali': tanggalKembali,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengembalian');
        } else {
          Info.snackbar('Gagal Update Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deletePengembalian(String kodePengembalian) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengembalian),
          body: {'kodePengembalian': kodePengembalian});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Menghapus Pengembalian');
        } else {
          Info.snackbar('Gagal Menghapus Pengembalian');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
